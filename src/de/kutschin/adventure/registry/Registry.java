package de.kutschin.adventure.registry;

import java.util.Set;

public interface Registry {
	
	public void register(String key, RegisterEntry registerEntry);
	public void unregister(String key);
	public Set<String> getRegisteredEntrieKeys();
	public RegisterEntry getRegisteredEntry(String key);
	
}
