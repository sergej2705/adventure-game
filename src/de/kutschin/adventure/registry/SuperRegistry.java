package de.kutschin.adventure.registry;

import java.util.HashMap;
import java.util.Set;

public class SuperRegistry implements Registry {
	
	private HashMap<String, RegisterEntry> registry = new HashMap<String, RegisterEntry>();

	public SuperRegistry() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void register(String key, RegisterEntry registerEntry) {
		
		this.registry.put(key, registerEntry);

	}

	@Override
	public void unregister(String key) {
		
		this.registry.remove(key);

	}

	@Override
	public Set<String> getRegisteredEntrieKeys() {
		
		return this.registry.keySet();
		
	}

	@Override
	public RegisterEntry getRegisteredEntry(String key) {
		// TODO Auto-generated method stub
		return null;
	}

}
